export const namespace = 'EDITING';

export const START = `${namespace}/START`;
export const FINISH = `${namespace}/FINISH`;

export const start = taskID => ({
	type: START,
	payload: { taskID },
});

export const finish = () => ({
	type: FINISH,
});