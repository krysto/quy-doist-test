import * as editing from './editing';

export const namespace = 'TASK';

export const UPDATE = `${namespace}/UPDATE`;
export const ADD = `${namespace}/ADD`;
export const REMOVE = `${namespace}/REMOVE`;
export const DONE = `${namespace}/DONE`;
export const UNDONE = `${namespace}/UNDONE`;
export const EXPAND = `${namespace}/EXPAND`;
export const COLLAPSE = `${namespace}/COLLAPSE`;
export const INDENT = `${namespace}/INDENT`;
export const OUTDENT = `${namespace}/OUTDENT`;
export const MOVE = `${namespace}/MOVE`;

export const update = (taskID, taskContent) => ({
	type: UPDATE,
	payload: { taskID, taskContent },
});

export const add = parentID => 
	(dispatch, getState) => {
		const state = getState()
		const taskID = state.todoList.nextID;

		dispatch({
			type: ADD,
			payload: { 
				parentID,
				task: {
					id: taskID,
					content: '',
					done: false,
					subTasks: [],
				}
			},
		});

		dispatch({
			type: editing.START,
			payload: { taskID },
		})
	};

export const remove = (taskID, parentID) => ({
	type: REMOVE,
	payload: { taskID, parentID },
});

export const done = taskID => ({
	type: DONE,
	payload: { taskID },
});

export const undone = taskID => ({
	type: UNDONE,
	payload: { taskID },
});

export const expand = taskID => ({
	type: EXPAND,
	payload: { taskID },
});

export const collapse = taskID => ({
	type: COLLAPSE,
	payload: { taskID },
});

// Indenting a task is assigning it as a subtask of its closest previous sibling.
// If no closest previous sibling task is found, do nothing.
export const indent = (taskID, parentID) =>
	(dispatch, getState) => {
		const { todoList: { tasks } } = getState();

		// Find the task's index in parent's subtasks
		const sibblingIDs = tasks[parentID].subTasks;
		const taskPos = sibblingIDs.indexOf(taskID);

		// Do nothing if not found or there're no previous sibling
		if (taskPos > 0) {
			// The closest previous sibling becomes its new parent
			const newParentID = sibblingIDs[taskPos - 1];

			// Move the task
			dispatch({
				type: MOVE,
				payload: {
					taskID,
					oldParentID: parentID,
					newParentID,
				}
			});
		}
	};

// Outdenting a task is assigning it as a subtask of its grand parent (parent's parent),
// positioned right after its current parent; and assigning all the next siblings as its subtasks
// If no grand parent is found, do nothing.
export const outdent = (taskID, parentID) =>
	(dispatch, getState) => {
		const { todoList: { tasks } } = getState();

		// Find grand parent's ID
		const grandParentID = Object.keys(tasks).find(
			id => tasks[id] && tasks[id].subTasks.indexOf(parentID) >= 0
		);

		if (grandParentID) {
			// Find parent's index in grand parent's subtasks
			const parentPos = tasks[grandParentID].subTasks.indexOf(parentID);

			// Find task's index in parent's subtasks
			const taskPos = tasks[parentID].subTasks.indexOf(taskID);

			// ID list of sibling tasks that come after the specified task
			const nextSiblingIDs = tasks[parentID].subTasks.slice(taskPos + 1);

			// Outdent the task
			dispatch({
				type: MOVE,
				payload: {
					taskID,
					oldParentID: parentID,
					newParentID: grandParentID,
					position: parentPos + 1,
				}
			});

			// Siblings come after the task become its subtasks
			nextSiblingIDs.forEach(id => dispatch({
				type: MOVE,
				payload: {
					taskID: id,
					oldParentID: parentID,
					newParentID: taskID,
				}
			}));
		}
	};

export const move = (taskID, oldParentID, newParentID, position) => ({
	type: MOVE,
	payload: {
		taskID,
		oldParentID,
		newParentID,
		position,
	}
});