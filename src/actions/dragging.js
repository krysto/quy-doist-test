export const namespace = 'DRAGGING';

export const START = `${namespace}/START`;
export const FINISH = `${namespace}/FINISH`;
export const HOVER_TARGET = `${namespace}/HOVER_TARGET`;
export const HOVER_INDENT = `${namespace}/HOVER_INDENT`;

export const start = (taskID, index, indentation) => ({
	type: START,
	payload: { taskID, index, indentation },
});

export const finish = () => ({
	type: FINISH,
});

export const hoverTarget = index => ({
	type: HOVER_TARGET,
	payload: { index },
});

export const hoverIndent = indentation => ({
	type: HOVER_INDENT,
	payload: { indentation },
});