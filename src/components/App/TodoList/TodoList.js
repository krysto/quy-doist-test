import React, { PureComponent, PropTypes } from 'react';
import BEM from 'react-bem-helper';
import { rootID } from 'constants';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import Task from './Task';

import './TodoList.styl';

const bem = BEM('todoList');

class TodoList extends PureComponent {
	static propTypes = {
		taskList: PropTypes.array.isRequired,
		addTask: PropTypes.func.isRequired,
	};

	render() {
		const { taskList } = this.props;

		return (
			<div {...bem()}>
				{taskList.map(
					({ taskID, indentation, hoverIndent, parentID, moveTargets }, listIndex) =>
						<Task key={taskID} 
							taskID={taskID} 
							parentID={parentID}
							indentation={indentation} 
							hoverIndent={hoverIndent}
							moveTargets={moveTargets}
							listIndex={listIndex}
						/>
				)}
				
				<div className="text-center">
					<button {...bem('newTaskBtn')}
						type="button" 
						onClick={this._handleNewTask}
					>
						New Task
					</button>
				</div>
			</div>
		);
	}

	_handleNewTask = () => {
		this.props.addTask(rootID);
	};
};

export default DragDropContext(HTML5Backend)(TodoList);