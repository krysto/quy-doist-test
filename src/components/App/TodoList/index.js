import { connect } from 'react-redux';
import { createStructuredSelector, createSelector } from 'reselect';
import * as actions from 'actions';
import { rootID } from 'constants';

import TodoList from './TodoList';

// Flatten tree structure to list of indented tasks.
// Displaying tasks in a flattened list makes it easier for drag & drop.
const flatten = (tasks, dragging) => {
	let draggedTask = null;

	const flattenSubTasks = (parent, indentation) => 
		parent.subTasks.reduce(
			(list, taskID) => {
				const taskInfo = { 
					taskID, 
					indentation, 
					parentID: parent.id,
				};

				if (taskID !== dragging.taskID) {
					// Add task info to the flattened list
					list.push(taskInfo); 

					// Recursively add its subtasks info to the flattened list
					// If a task is collapsed, don't render its subtasks
					if (!tasks[taskID].collapsed) {
						list.push(
							...flattenSubTasks(tasks[taskID], indentation + 1)
						);
					}

				} else {
					// Add dragged task to the hover position alter
					draggedTask = taskInfo;
				}

				return list;
			}, 
			[]
		);

	// Flattened list excluding the dragged task
	let list = flattenSubTasks(tasks[rootID], 0);

	// If one task is being dragged
	if (dragging.taskID) {
		// The task may be dropped into one of the move targets.
		// Each moveTargets item contains information of where
		// to move the dragged task to if it's dropped into the move target.
		const { hoverIndex } = dragging;
		const moveTargets = [];

		const maxIndent = hoverIndex === 0 ? 
			0 : list[hoverIndex - 1].indentation + 1;

		for (let indent = 0; indent <= maxIndent; indent++) {
			let parentID = rootID;
			let position = 0;

			for (let i = hoverIndex - 1; i >= 0; i--) {
				// Parent task is the last line with smaller indentation
				if (list[i].indentation < indent) {
					parentID = list[i].taskID;
					break;

				// Subtask position is the number of tasks with the 
				// same indentation until the parent is encountered.
				} else if (list[i].indentation === indent) {
					position++;
				}
			}

			moveTargets.push({ parentID, position });
		}

		draggedTask.moveTargets = moveTargets;

		draggedTask.hoverIndent = dragging.hoverIndent;

		// Place dragged task info to the current hover position
		list.splice(hoverIndex, 0, draggedTask);
	}

	return list;
};

const mapStateToProps = createStructuredSelector({
	taskList: createSelector(
		state => state.todoList.tasks,
		state => state.dragging,
		flatten
	),
});

const usedActions = {
	addTask: actions.task.add,
};

export default connect(mapStateToProps, usedActions)(TodoList);