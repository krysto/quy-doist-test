import { connect } from 'react-redux';
import * as actions from 'actions';

import TaskDraft from './TaskDraft';

const usedActions = {
	updateTask: actions.task.update,
	removeTask: actions.task.remove,
	indentTask: actions.task.indent,
	outdentTask: actions.task.outdent,
	addTask: actions.task.add,
};

export default connect(null, usedActions)(TaskDraft);