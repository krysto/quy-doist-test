import React, { PureComponent, PropTypes } from 'react';

import './TaskDraft.styl';

export default class TaskDraft extends PureComponent {
	static propTypes = {
		currentContent: PropTypes.string,
		taskID: PropTypes.number.isRequired,
		parentID: PropTypes.number.isRequired,
		finishEditing: PropTypes.func.isRequired,
		updateTask: PropTypes.func.isRequired,
		removeTask: PropTypes.func.isRequired,
		indentTask: PropTypes.func.isRequired,
		outdentTask: PropTypes.func.isRequired,
		addTask: PropTypes.func.isRequired,
	};

	static defaultProps = {
		currentContent: '',
	};

	render() {
		const { currentContent } = this.props;

		return (
			<input
				ref="input"
				type="text"
				placeholder="What is your plan?"
				className="task__taskDraft fullWidth"
				autoFocus
				defaultValue={currentContent}
				onKeyDown={this._handleKeyDown}
				onBlur={this._handleBlur}
			/>
		);
	}

	_handleKeyDown = event => {
		const { taskID, parentID, currentContent, indentTask, outdentTask, addTask } = this.props;

		if (event.key === 'Escape') {
			this._cancelChanges();

		} else if (event.key === 'Enter') {
			this._commitChanges();
			if (!currentContent) {
				addTask(parentID);
			}

		} else if (event.key === 'Tab') {
			event.preventDefault();

			if (event.shiftKey) {
				outdentTask(taskID, parentID);

			} else {
				indentTask(taskID, parentID);
			}
		}
	};

	_handleBlur = event => {
		setTimeout(this._commitChanges, 0);
	};

	_cancelChanges = () => {
		const { taskID, parentID, currentContent, finishEditing, removeTask } = this.props;

		if (!currentContent) {
			removeTask(taskID, parentID);
		}

		finishEditing();
	};

	_commitChanges = () => {
		const { taskID, parentID, finishEditing, updateTask, removeTask } = this.props;
		const draftContent = this.refs.input.value;

		if (draftContent) {
			updateTask(taskID, draftContent);
		} else {
			removeTask(taskID, parentID);
		}

		finishEditing();
	};
};