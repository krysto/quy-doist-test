import React, { PureComponent, PropTypes } from 'react';
import BEM from 'react-bem-helper';

import DragHandler from './DragHandler';
import Expander from './Expander';
import NewSubTaskButton from './NewSubTaskButton';
import DoneCheckbox from './DoneCheckbox';
import TaskDraft from './TaskDraft';
import MoveTarget from './MoveTarget';

import './Task.styl';

const bem = BEM('task');

export default class Task extends PureComponent {
	static propTypes = {
		// passed by parent
		parentID: PropTypes.number.isRequired,
		indentation: PropTypes.number.isRequired,
		hoverIndent: PropTypes.number,
		listIndex: PropTypes.number.isRequired,
		moveTargets: PropTypes.array,

		// connected from store
		task: PropTypes.object.isRequired,
		editing: PropTypes.bool.isRequired,

		// action creators
		addTask: PropTypes.func.isRequired,
		doneTask: PropTypes.func.isRequired,
		undoneTask: PropTypes.func.isRequired,
		expandTask: PropTypes.func.isRequired,
		collapseTask: PropTypes.func.isRequired,
		startEditing: PropTypes.func.isRequired,
		finishEditing: PropTypes.func.isRequired,

		// drag & drop props
		connectDragSource: PropTypes.func.isRequired,
		connectDragPreview: PropTypes.func.isRequired,
		dragging: PropTypes.bool.isRequired,
		connectDropTarget: PropTypes.func.isRequired,
	};

	render() {
		const { 
			task, parentID, editing, moveTargets, hoverIndent,
			connectDragSource, connectDragPreview, dragging, connectDropTarget,
		} = this.props;

		return connectDropTarget(connectDragPreview(
			<div {...bem(null)}>
				<div {...bem('body', { dragging })}>
					{this.renderIndentation()}

					{connectDragSource(
						<div {...bem('block')}>
							<DragHandler />
						</div>
					)}

					<div {...bem('block')}>
						{!!task.subTasks.length &&
							<Expander expanded={!task.collapsed && !dragging} 
								onClick={this._toggleExpand} 
							/>
						}
					</div>

					<div {...bem('block')}>
						<DoneCheckbox checked={!!task.done} onChange={this._toggleDone} />
					</div>

					{editing ?
						<TaskDraft taskID={task.id}
							parentID={parentID}
							currentContent={task.content}
							finishEditing={this._finishEditing}
						/>
						:
						<div {...bem('content', { done: task.done })} onClick={this._startEditing}>
							{task.content}
						</div>
					}

					{!!task.content &&
						<div {...bem('block')}>
							<NewSubTaskButton onClick={this._newSubTask} />
						</div>
					}
				</div>

				{dragging && moveTargets &&
					<div {...bem('dropLayer')}>
						{moveTargets.map((target, i) =>
							<MoveTarget key={i} moveTarget={target} 
								indent={i} 
								hoverIndent={hoverIndent}
								last={i === moveTargets.length - 1}
							/>
						)}
					</div>
				}
			</div>
		));
	}

	renderIndentation() {
		const { hoverIndent, indentation } = this.props;
		const indentBlocks = [];
		const indent = hoverIndent !== undefined ?
			hoverIndent : indentation;

		for (let i = 0; i < indent; i++) {
			indentBlocks.push(
				<div key={i} {...bem('block')} />
			);
		}

		return indentBlocks;
	}

	_startEditing = () => {
		this.props.startEditing(this.props.task.id);
	};

	_finishEditing = () => {
		this.props.finishEditing();
	};

	_toggleExpand = () => {
		const { task, expandTask, collapseTask } = this.props;

		if (task.collapsed) {
			expandTask(task.id);
		} else {
			collapseTask(task.id);
		}
	};

	_toggleDone = () => {
		const { task, doneTask, undoneTask } = this.props;

		if (task.done) {
			undoneTask(task.id);
		} else {
			doneTask(task.id);
		}
	};

	_newSubTask = () => {
		this.props.addTask(this.props.task.id);
	};
};
