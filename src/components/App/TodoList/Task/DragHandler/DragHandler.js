import React from 'react';

import SvgIcon from 'components/shared/SvgIcon';

import './DragHandler.styl';

export default function DragHandler() {
	return (
		<div className="task__dragHandler fullBoth bothCenter">
			<SvgIcon 
				icon="menu" 
				htmlAttr={{
					width: 16,
					height: 16,
				}} 
			/>
		</div>
	);
};