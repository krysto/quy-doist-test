import { DragSource } from 'react-dnd';

import Task from './Task';

const spec = {
	beginDrag: props => {
		const { startDragging, taskID, listIndex, parentID, indentation } = props;

		startDragging(taskID, listIndex, indentation);

		return {
			id: taskID,
			index: listIndex,
			parentID: parentID,
		};
	},

	endDrag: props => {
		props.finishDragging();
	},
};

const collect = (connect, monitor) => ({
	connectDragSource: connect.dragSource(),
	connectDragPreview: connect.dragPreview(),
	dragging: monitor.isDragging(),
});

export default DragSource('Task', spec, collect)(Task);