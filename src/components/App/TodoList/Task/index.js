import { connect } from 'react-redux';
import { createStructuredSelector, createSelector } from 'reselect';
import * as actions from 'actions';

import DroppableTask from './DroppableTask';

const mapStateToProps = createStructuredSelector({
	task: createSelector(
		(state) => state.todoList.tasks,
		(state, props) => props.taskID,
		(tasks, taskID) => tasks[taskID]
	),
	editing: (state, props) => state.editingTaskID === props.taskID,
});

const usedActions = {
	addTask: actions.task.add,
	doneTask: actions.task.done,
	undoneTask: actions.task.undone,
	expandTask: actions.task.expand,
	collapseTask: actions.task.collapse,

	startEditing: actions.editing.start,
	finishEditing: actions.editing.finish,

	startDragging: actions.dragging.start,
	finishDragging: actions.dragging.finish,
	hoverTarget: actions.dragging.hoverTarget,
};

const ConnectedTask = connect(mapStateToProps, usedActions)(DroppableTask);

export default ConnectedTask;