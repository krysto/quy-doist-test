import { DropTarget } from 'react-dnd';

import MoveTarget from './MoveTarget';

const spec = {
	hover: (props, monitor, component) => {
		const { indent, hoverIndent } = props;

		if (indent !== hoverIndent) {
			props.hoverIndent(indent);
		}
	},

	drop: (props, monitor, component) => {
		const { 
			moveTarget: { 
				parentID: newParentID, 
				position: newPosition
			}, 
			moveTask,
		} = props;

		const { id, parentID: oldParentID } = monitor.getItem();

		props.moveTask(id, oldParentID, newParentID, newPosition);
	}
};

const collect = (connect, monitor) => ({
	connectDropTarget: connect.dropTarget()
});

export default DropTarget('Task', spec, collect)(MoveTarget);