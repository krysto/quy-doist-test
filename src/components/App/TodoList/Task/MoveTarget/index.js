import { connect } from 'react-redux';
import * as actions from 'actions';

import DroppableMoveTarget from './DroppableMoveTarget';

const usedActions = {
	moveTask: actions.task.move,
	hoverIndent: actions.dragging.hoverIndent,
};

export default connect(null, usedActions)(DroppableMoveTarget);