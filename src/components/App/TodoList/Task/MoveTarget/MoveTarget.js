import React, { PureComponent, PropTypes } from 'react';
import BEM from 'react-bem-helper';

const bem = BEM('task');

export default class MoveTarget extends PureComponent {
	static propTypes = {
		connectDropTarget: PropTypes.func.isRequired,
		last: PropTypes.bool.isRequired,
	};

	render() {
		const { connectDropTarget, last } = this.props;

		return connectDropTarget(
			<div {...bem('block', null, { flexFill: last })}>
				<div className="fullBoth bothCenter" />
			</div>
		);
	}
};