import React, { PropTypes } from 'react';

import SvgIcon from 'components/shared/SvgIcon';

import './DoneCheckbox.styl';

export default function DoneCheckbox({ checked, onChange }) {
	return (
		<label className="displayBlock fullBoth bothCenter cursorPointer">
			<input type="checkbox" 
				className="hidden"
				checked={checked}
				onChange={onChange}
			/>
			<SvgIcon 
				icon={`checkbox-${checked ? '' : 'un'}checked`} 
				htmlAttr={{
					width: 30,
					height: 30,
				}}
			/>
		</label>
	);
};

DoneCheckbox.propTypes = {
	checked: PropTypes.bool.isRequired,
	onChange: PropTypes.func.isRequired,
};