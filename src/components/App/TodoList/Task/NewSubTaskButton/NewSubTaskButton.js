import React, { PropTypes } from 'react';
import BEM from 'react-bem-helper';

import SvgIcon from 'components/shared/SvgIcon';

import './NewSubTaskButton.styl';

const bem = BEM('task__newSubTask');

export default function NewSubTaskButton({ onClick }) {
	return (
		<div {...bem(null, null, 'fullBoth bothCenter cursorPointer')} onClick={onClick}>
			<SvgIcon 
				icon="plus" 
				htmlAttr={{
					width: 16,
					height: 16,
				}}
			/>
		</div>
	);
};

NewSubTaskButton.propTypes = {
	onClick: PropTypes.func.isRequired,
};