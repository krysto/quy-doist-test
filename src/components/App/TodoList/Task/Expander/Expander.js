import React, { PropTypes } from 'react';
import BEM from 'react-bem-helper';

import SvgIcon from 'components/shared/SvgIcon';

import './Expander.styl';

const bem = BEM('task__expander');

export default function Expander({ expanded, onClick }) {
	return (
		<div {...bem(null, null, 'fullBoth bothCenter cursorPointer')} onClick={onClick}>
			<SvgIcon 
				icon="arrow-right-fill" 
				htmlAttr={{
					...bem('arrow', { expanded }),
					width: 16,
					height: 16,
				}}
			/>
		</div>
	);
};

Expander.propTypes = {
	expanded: PropTypes.bool.isRequired,
	onClick: PropTypes.func.isRequired,
};