import React from 'react';
import TodoList from './TodoList';

import 'normalize-css/normalize.css';
import './App.styl';
import './common.styl';

export default function App() {
	return (
		<div>
			<TodoList />
		</div>
	);
};