import createReducer from 'helpers/createReducer';
import * as actions from 'actions';

const initialState = null;

export default createReducer(
	initialState, 
	{
		[actions.editing.START]: (state, { payload: { taskID } }) =>
			taskID,

		[actions.editing.FINISH]: () =>
			initialState,
	}
);
