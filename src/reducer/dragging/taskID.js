import createReducer from 'helpers/createReducer';
import * as actions from 'actions';

// ID of the task being dragged

const initialState = null;

export default createReducer(
	initialState, 
	{
		[actions.dragging.START]: (state, { payload: { taskID } }) =>
			taskID,

		[actions.dragging.FINISH]: () =>
			initialState,
	}
);
