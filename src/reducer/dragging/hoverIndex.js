import createReducer from 'helpers/createReducer';
import * as actions from 'actions';

// Index of the task that is being hovered in the flattened list

const initialState = null;

export default createReducer(
	initialState, 
	{
		[actions.dragging.START]: (state, { payload: { index } }) =>
			index,

		[actions.dragging.HOVER_TARGET]: (state, { payload: { index } }) =>
			index,

		[actions.dragging.FINISH]: () =>
			initialState,
	}
);
