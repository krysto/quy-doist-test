import createReducer from 'helpers/createReducer';
import * as actions from 'actions';

// Indent level that being hovered 

const initialState = null;

export default createReducer(
	initialState, 
	{
		[actions.dragging.START]: (state, { payload: { indentation } }) =>
			indentation,

		[actions.dragging.HOVER_INDENT]: (state, { payload: { indentation } }) =>
			indentation,

		[actions.dragging.FINISH]: () =>
			initialState,
	}
);
