import { combineReducers } from 'redux';
import taskID from './taskID';
import hoverIndex from './hoverIndex';
import hoverIndent from './hoverIndent';

export default combineReducers({
	taskID,
	hoverIndex,
	hoverIndent,
});