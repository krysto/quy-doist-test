import { combineReducers } from 'redux';
import todoList from './todoList';
import editingTaskID from './editingTaskID';
import dragging from './dragging';

export default combineReducers({
	todoList,
	editingTaskID,
	dragging,
});