import { combineReducers } from 'redux';
import tasks from './tasks';
import nextID from './nextID';

export default combineReducers({
	tasks,
	nextID,
});