import update from 'immutability-helper';
import createReducer from 'helpers/createReducer';
import { rootID } from 'constants';
import * as actions from 'actions';

const initialState = {
	[rootID]: {
		id: rootID,
		isRoot: true,
		subTasks: [],
	}
};

export default createReducer(
	initialState, 
	{ 
		[actions.task.ADD]: (state, { payload: { parentID, task } }) =>
			update(state, {
				// add task to task list
				[task.id]: {
					$set: task
				},
				// add task as a parent's subtask
				[parentID]: {
					subTasks: {
						$push: [task.id]
					}
				}
			}),

		[actions.task.UPDATE]: (state, { payload: { taskID, taskContent } }) =>
			update(state, {
				[taskID]: {
					content: {
						$set: taskContent
					}
				}
			}),

		[actions.task.REMOVE]: (state, { payload: { taskID, parentID } }) => {
			// remove task from task list
			const updates = {
				[taskID]: {
					$set: undefined
				}
			};

			// remove task from parent's subtasks
			const index = state[parentID].subTasks.indexOf(taskID);

			if (index >= 0) {
				updates[parentID] = {
					subTasks: {
						$splice: [[ index, 1 ]]
					}
				};
			}

			return update(state, updates);
		},

		[actions.task.DONE]: (state, { payload: { taskID } }) =>
			update(state, {
				[taskID]: {
					done: {
						$set: true
					}
				}
			}),

		[actions.task.UNDONE]: (state, { payload: { taskID } }) =>
			update(state, {
				[taskID]: {
					done: {
						$set: false
					}
				}
			}),

		[actions.task.EXPAND]: (state, { payload: { taskID } }) =>
			update(state, {
				[taskID]: {
					collapsed: {
						$set: false
					}
				}
			}),

		[actions.task.COLLAPSE]: (state, { payload: { taskID } }) =>
			update(state, {
				[taskID]: {
					collapsed: {
						$set: true
					}
				}
			}),

		[actions.task.MOVE]: (state, { payload: { taskID, oldParentID, newParentID, position } }) => {
			const oldParent = state[oldParentID];
			const removeIndex = oldParent.subTasks.indexOf(taskID);
			
			const newParent = state[newParentID];
			const insertIndex = position !== undefined ? 
				position : newParent.subTasks.length;

			if (oldParentID === newParentID && removeIndex === insertIndex) {
				return state;
			}

			if (oldParentID === newParentID) {
				return update(state, {
					[oldParentID]: {
						subTasks: {
							$splice: [
								[ removeIndex, 1 ],
								[ insertIndex, 0, taskID ]
							]
						}
					}
				});
			} else {
				return update(state, {
					// remove task from old parent's subtasks
					[oldParentID]: {
						subTasks: {
							$splice: [[ removeIndex, 1 ]]
						}
					},
					// insert taskID to new parent's subtasks at the specified position
					[newParentID]: {
						subTasks: {
							$splice: [[ insertIndex, 0, taskID ]]
						}
					}
				});
			}
		},
	}
);