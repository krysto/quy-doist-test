import createReducer from 'helpers/createReducer';
import { rootID } from 'constants';
import * as actions from 'actions';

const initialState = rootID + 1;

export default createReducer(
	initialState, 
	{
		[actions.task.ADD]: state => state + 1
	}
);