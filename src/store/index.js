import { applyMiddleware, createStore, compose } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import reducer from 'reducer';
import { storeDataMiddleware, loadStoredData } from './storeDataMiddleware';

export default function(initialState) {
	let state = initialState;
	const middlewares = [thunk];

	if (typeof localStorage !== 'undefined') {
		middlewares.push(storeDataMiddleware);
		state = loadStoredData(state);
	}

	if (process.env.NODE_ENV !== 'production') {
		middlewares.push(createLogger());
	}

	const composeEnhancers = (
		process.env.NODE_ENV !== 'production' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
	) || compose;

	const store = createStore(reducer, state, composeEnhancers(
		applyMiddleware(...middlewares)
	));

	return store;
}