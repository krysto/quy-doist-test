import update from 'immutability-helper';

export const storeDataMiddleware = store => next => action => {
	const currentState = store.getState();
	const currentTodoList = currentState.todoList;

	const result = next(action);

	const newState = store.getState();
	const newTodoList = newState.todoList;

	if (newTodoList !== currentTodoList) {
		localStorage.setItem('todoList', JSON.stringify(newTodoList));
	}

	return result;
};

export function loadStoredData(initialState) {
	const storedData = localStorage.getItem('todoList');

	if (storedData) {
		const todoList = JSON.parse(storedData);

		return update(initialState, {
			todoList: {
				$set: todoList
			}
		});
	}

	return initialState;
};