
# Doist test project

Author: Hoang Phu Quy

Below are some notes on my project:

## UI & UX

- The UX aspect of the example app in the video is already pretty good, so I keep most of its basic behaviours, just cut some out-of-scope features and add some minor additions.
- Added a "plus" button to quickly add a subtask to a given task. If I had more time I may add a tooltip to the button to explain what it does.
- When you compose a task, you can indent and outdent it using Tab and Shift+Tab. 
- Hitting Enter when editing a task will commit the changes, and Escape will cancel the changes.
- You can drag a task to move it. There're 2 known issues here that I will fix if I had more time.
  + You cannot drag a task you're editing or creating which has empty textbox, because focusing out of the textbox will commit the changes immediately, and if the task content is empty, it will be deleted.
  + Moving tasks may not work correctly if the move target is collapsed.

## Redux state structure & actions

- I store todo tasks in a normalized tree, no nesting structures, subtasks are referenced by their IDs to eliminate redundancy and make it easier to find and update a single task.
- Root is an empty pseudo task, only used to store the list of first indent level tasks. 
- Although being stored in a normalized tree in Redux state, task list shown in the UI is flattened and indented by tasks' depth in the tree. Flattening the tree makes it easier to move tasks.

## Running the project

```
$ yarn install
$ yarn start
```
